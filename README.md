# vchunk

_vchunk_ is a tiny [bchunk](https://github.com/hessu/bchunk) clone written in [V](https://vlang.io/).

## Features

* Cue Sheets
* Modes:
  * MODE1/2352
	* MODE2/2352:
    * RAW
    * PSX
    * Classic
  * MODE2/2336
  * AUDIO
    * Output to Wave files
    * Swap Audio data

## Building

```shell
v -prod .
```

## Running

```shell
./vchunk -cue PSX_Disc.cue
Reading the CUE file:

Track 1: MODE2/2352 01 00:00:00

Writing tracks:

01 -> PSX_Disc_01.iso
```

### Exporting Compact Disc Digital Audio data to Wave

Use the `-wav` or `-w` flag.

If audio tracks became only senseless static noise, use the `-swap` or `-s` flag
to flip back audio bits.

```shell
./vchunk -wav -cue ~/Audio Disc.cue
Reading the CUE file:

Track 1: AUDIO 01 00:00:00
Track 2: AUDIO 01 04:02:33
Track 3: AUDIO 01 07:56:33
Track 4: AUDIO 01 11:38:69
Track 5: AUDIO 01 16:23:48
Track 6: AUDIO 01 20:35:11
Track 7: AUDIO 01 24:00:24
Track 8: AUDIO 01 28:58:40
Track 9: AUDIO 01 32:52:34
Track 10: AUDIO 01 37:41:01
Track 11: AUDIO 01 41:37:70
Track 12: AUDIO 01 46:33:36

Writing tracks:

01 -> Audio Disc_01.wav
02 -> Audio Disc_02.wav
03 -> Audio Disc_03.wav
04 -> Audio Disc_04.wav
05 -> Audio Disc_05.wav
06 -> Audio Disc_06.wav
07 -> Audio Disc_07.wav
08 -> Audio Disc_08.wav
09 -> Audio Disc_09.wav
10 -> Audio Disc_10.wav
11 -> Audio Disc_11.wav
12 -> Audio Disc_12.wav
```
