import cli
import os
import v.vmod

const (
	extension_iso        = 'iso'
	extension_cdr        = 'cdr'
	extension_wav        = 'wav'
	extension_unknown    = 'ugh'
	sector_length        = u64(2352)
	type_header_length   = 4
	format_header_length = 24
	data_header_length   = 8
	riff_header_length   = 4 + type_header_length + format_header_length + data_header_length
)

struct Track {
	num  int
	mode string
mut:
	audio        bool
	extension    string
	data_start   int
	data_size    int
	start_sector u64
	stop_sector  u64
	start        u64
	stop         u64
}

fn main() {
	vm := vmod.decode(@VMOD_FILE) or { panic(err) }
	mut app := cli.Command{
		name: '${vm.name}'
		description: '${vm.description}'
		version: '${vm.version}'
		execute: run
		flags: [
			cli.Flag{
				flag: .bool
				name: 'verbose'
				abbrev: 'v'
				description: 'Verbose Mode'
			},
			cli.Flag{
				flag: .string
				name: 'cue'
				abbrev: 'c'
				description: 'CUE file'
				required: true
			},
			cli.Flag{
				flag: .string
				name: 'basename'
				abbrev: 'b'
				description: 'Base filename for output files'
			},
			cli.Flag{
				flag: .bool
				name: 'wav'
				abbrev: 'w'
				description: 'WAV Mode: Output audio files in WAV format'
			},
			cli.Flag{
				flag: .bool
				name: 'swap'
				abbrev: 's'
				description: 'WAV Mode: Swap audio data'
			},
			cli.Flag{
				flag: .bool
				name: 'raw'
				abbrev: 'r'
				description: 'Raw Mode for MODE2/2352: write all 2352 bytes from offset 0 (VCD/MPEG)'
			},
			cli.Flag{
				flag: .bool
				name: 'psx'
				abbrev: 'p'
				description: 'PSX Mode for MODE2/2352: write 2336 bytes from offset 24'
			},
		]
	}
	app.setup()
	app.parse(os.args)
}

fn get_track_mode(mut track Track, wav_mode bool, raw_mode bool, psx_mode bool) {
	match track.mode {
		'MODE1/2352' {
			track.data_start = 16
			track.data_size = 2048
			track.extension = extension_iso
		}
		'MODE2/2352' {
			if raw_mode {
				track.data_start = 0
				track.data_size = 2352
			} else if psx_mode {
				track.data_start = 0
				track.data_size = 2336
			} else {
				track.data_start = 24
				track.data_size = 2048
			}
			track.extension = extension_iso
		}
		'MODE2/2336' {
			track.data_start = 16
			track.data_size = 2336
			track.extension = extension_iso
		}
		'AUDIO' {
			track.data_start = 0
			track.data_size = 2352
			track.audio = true
			if wav_mode {
				track.extension = extension_wav
			} else {
				track.extension = extension_cdr
			}
		}
		else {
			track.data_start = 0
			track.data_size = 2352
			track.extension = extension_unknown
		}
	}
}

fn time_to_frames(s string) u64 {
	time_data := s.split(':')
	mins := time_data[0].u64()
	secs := time_data[1].u64()
	frames := time_data[2].u64()
	return 75 * (mins * 60 + secs) + frames
}

fn write_track(mut input os.File, track Track, name string, wav_mode bool, swap_mode bool, verbose bool) {
	output_filename := '${name}_${track.num:02}.${track.extension}'

	println('${track.num:02} -> ${output_filename}')

	mut output := os.open_file(output_filename, 'wb') or {
		eprintln('Failed to open output file "${output_filename}": ${err}')
		return
	}
	defer {
		output.close()
	}

	real_length := (track.stop_sector - track.start_sector + 1) * u64(track.data_size)
	if verbose {
		mmc_sectors := track.stop_sector - track.start_sector + 1
		mmc_bytes := track.stop - track.start + 1
		println('\tMMC sectors ${track.start_sector} -> ${track.stop_sector} (${mmc_sectors})')
		println('\tMMC bytes ${track.start} -> ${track.stop} (${mmc_bytes})')
		println('\tSector data at ${track.data_start}, ${track.data_size} bytes per sector')
		println('\tReal data ${real_length} bytes')
	}

	// Do we want Wave audio files ?
	if track.audio && wav_mode {
		// RIFF header
		output.write_string('RIFF') or { panic(err) }
		output.write_raw(u32(riff_header_length - 4 + real_length)) or { panic(err) }
		// TYPE header
		output.write_string('WAVE') or { panic(err) }
		// FORMAT header
		output.write_string('fmt ') or { panic(err) } // Chunk header
		output.write_raw(u32(16)) or { panic(err) } // Size of chunk
		output.write_raw(u16(1)) or { panic(err) } // Type of format: PCM
		output.write_raw(u16(2)) or { panic(err) } // Number of channels: Stereo
		output.write_raw(u32(44100)) or { panic(err) } // Sample rate: 44100 (CD)
		output.write_raw(u32(44100 * 2 * (2 * 8) / 8)) or { panic(err) } // Byte rate: Sample rate * Channels * BitsPerSample / 8
		output.write_raw(u16(2 * (2 * 8) / 8)) or { panic(err) } // Block align: Channels * BitsPerSample / 8
		output.write_raw(u16(2 * 8)) or { panic(err) } // Bits per sample: 16 bits
		// DATA header
		output.write_string('data') or { panic(err) } // Chunk header
		output.write_raw(u32(real_length)) or { panic(err) } // Size of chunk
	}

	mut sector := track.start_sector
	mut buf := []u8{len: int(sector_length)}
	input.seek(i64(track.start), os.SeekMode.start) or { panic(err) }
	for {
		count := input.read(mut buf) or { panic(err) }
		if count == 0 {
			break
		}

		// Red Book says that:
		// The audio contained in a CD-DA consists of two-channel signed 16-bit LPCM sampled at
		// 44,100 Hz and written as a little-endian interleaved stream with left channel coming
		// first.
		//
		// BUT.
		// Sometime softwares behave wrongly. Like cdrdao that will swap bytes to big-endian while
		// reading audio tracks on ANY little-endian system. And so does all software based on cdrdao.
		// This have been fixed in crdao release 1.2.5.
		//
		// So we consider that by default we should not swap audio bits.
		if track.audio && swap_mode {
			mut idx := track.data_start
			for idx < track.data_size {
				buf[idx], buf[idx + 1] = buf[idx + 1], buf[idx]
				idx += 2
			}
		}

		output.write(buf[track.data_start..(track.data_start + track.data_size)]) or { panic(err) }

		sector++
		if sector > track.stop_sector {
			break
		}
	}
}

fn parse_cue_command(line string) []string {
	mut command := []string{}

	mut quoted_token := []string{}
	mut in_quote := false
	for token in line.trim_space().split(' ') {
		if in_quote {
			quoted_token << token
			if token.ends_with('"') {
				in_quote = false
				command << quoted_token.join(' ')#[1..-1]
			}
		} else {
			if token.starts_with('"') {
				if !token.ends_with('"') {
					in_quote = true
					quoted_token << token
				} else {
					command << token#[1..-1]
				}
			} else {
				command << token
			}
		}
	}
	return command
}

fn get_binary_path(cue_dir string, bin_filename string) string {
	mut bin_path := bin_filename
	if !bin_filename.starts_with('/') {
		// BIN file path is not an UNIX absolute path
		if bin_filename.starts_with('.') || bin_filename.starts_with('..') {
			// Path is a relative UNIX or DOS path

			if bin_filename.trim_left('.')[0].ascii_str() == '/' {
				// UNIX Path
				$if windows {
					bin_path = bin_filename.replace('/', '\\')
				}
			} else {
				// DOS Path
				$if !windows {
					bin_path = bin_filename.replace('\\', '/')
				}
			}
			bin_path = os.join_path_single(cue_dir, bin_path)
		} else if bin_filename[1].ascii_str() != ':' {
			// Not a DOS absolute path either
			if !cue_dir.is_blank() {
				bin_path = os.join_path_single(cue_dir, bin_path)
			}
		}
	}
	return bin_path
}

fn parse_cue_file(cue_file string, wav_mode bool, raw_mode bool, psx_mode bool, verbose bool) !(string, []&Track) {
	// CUE file Documentation:
	// 	https://web.archive.org/web/20070614044112/http://www.goldenhawk.com/download/cdrwin.pdf

	println('Reading the CUE file:')
	println('')

	mut current_track := -1
	mut bin_file := ''
	mut bin_file_size := u64(0)
	mut tracks := []&Track{}

	cue_dir := cue_file.trim_string_right(os.base(cue_file))
	lines := os.read_lines(cue_file)!

	for line in lines {
		command := parse_cue_command(line)

		if command[0] == 'FILE' {
			// Syntax
			// 	FILE filename filetype
			//
			// Arguments
			//	filename: The audio or daat file's filename (can include device and directory).
			//			  If the filename contains any spaces, the filename must be enclosed in
			//			  quotation marks.
			//	filetype: The audio or data file's filetype. The following audio filetypes are
			//			  allowed:
			//				BINARY:	Intel binary file (least significant byte first). Use for
			//						data files.
			//				MOTOROLA: Motorola binary file (most significant byte first). Use for
			//						data files.
			//				AIFF:	Audio AIFF file (44.1KHz 16-bit stereo)
			//				WAVE:	Audio WAVE file (44.1KHz 16-bit stereo)
			//				MP3:	Audio MP3 file (44.1KHz 16-bit stereo)
			//
			// Examples
			//	FILE C:\DATA\TRACK1.ISO BINARY
			//	FILE C:\MUSIC\TRACK2.WAV WAVE
			//	FILE "C:\MUSIC\LONG FILENAME.MP3" MP3
			//
			// Restrictions
			//	FILE commands must appear before any other command, except CATALOG and CDTEXTFILE.
			//	For AUDIO files, if the length of the data within the file is not an exact multiple
			//	of the CDROM sector size (2352 bytes), CDRWIN pads the last sector with zeros when
			//	it is recorded to the blank disc.
			bin_file = get_binary_path(cue_dir, command[1])

			if !os.exists(bin_file) {
				eprintln('Failed: Binary file "${bin_file}" does not exists.')
				exit(1)
			}

			bin_file_size = os.file_size(bin_file)
			if bin_file_size == 0 {
				eprintln('Failed to read binary file size.')
				exit(1)
			}
			continue
		}

		if bin_file == '' {
			match command[0] {
				'CATALOG', 'CDTEXTFILE' {}
				else {
					eprintln('Received command ${command[0]} but no FILE was specified. Aborting.')
					exit(1)
				}
			}
		}

		match command[0] {
			'REM' {
				// Syntax
				// 	REM comment
				//
				// Argument
				// comment: Your comments. There is no restriction on how long your comments can
				//			be, but successive lines of comments must begin with the REM command.
				//
				// Example
				//	REM This is a
				//	REM long comment
			}
			'TRACK' {
				// Use the TRACK command to start a new TRACK
				//
				// Syntax
				//	TRACK number datatype
				//
				// Arguments
				//	number: Track number. The range is 1 to 99.
				// 	datatype: Track datatype. The following datatypes are allowed:
				//				AUDIO: Audio/Music (2352)
				//				CDG: Karaoke CD+G (2448)
				//				MODE1/2048: CD-ROM Mode1 Data (cooked)
				//				MODE1/2352: CD-ROM Mode1 Data (raw)
				//				MODE2/2336: CD-ROM XA Mode2 Data
				//				MODE2/2352: CD-ROM XA Mode2 Data
				//				CDI/2336: CD-I Mode2 Data
				//				CDI/2352: CD-I Mode2 Data
				//
				// Examples
				//	TRACK 1 MODE1/2048
				//	TRACK 20 AUDIO
				//
				// Restrictions
				//	All tracks numbers must be between 1 and 99 inclusive.
				//	The first track number can be greater than one, but all track numbers after
				//	the first must be sequential.
				//	You must specify at least one track per file.
				current_track++
				mut track := &Track{
					num: command[1].int()
					mode: command[2]
				}
				print('Track ${track.num}: ${track.mode} ')
				get_track_mode(mut track, wav_mode, raw_mode, psx_mode)
				tracks << track
			}
			'INDEX' {
				// You can use the INDEX command to specify indexes (or subindexes) within a track.
				//
				// Syntax
				//	INDEX number mm:ss:ff
				//
				// Arguments
				//	number: Index number. The range is 0 to 99.
				//	mm:ss:ff: Starting time of index in minutes (mm), seconds (ss) and frames (ff).
				//			  There are 75 frames per second. All times are relative to the
				//			  beginning of the current file.
				//
				// Examples
				//	INDEX 01 00:00:00
				//	INDEX 05 02:34:50
				//
				// Restrictions
				//	The first index must be 0 or 1 with all other indexes being sequential to the
				//	first one.
				//  The first index of a track must start at 00:00:00.
				//	INDEX 0 specifies the starting time of the track pregap.
				//	INDEX 1 specifies the starting time of the track data. Index 1 is the only
				//	index that is stored in the disc's table of contents.
				//	INDEX numbers greater than 1 specify a subindex within a track.
				mut track := tracks[current_track]
				track.start_sector = time_to_frames(command[2])
				track.start = track.start_sector * sector_length

				print('${command[1]} ${command[2]}')
				if verbose {
					println(' (Start sector: ${track.start_sector}, Offset: ${track.start})')
				} else {
					println('')
				}

				if current_track > 0 {
					mut previous_track := tracks[current_track - 1]
					previous_track.stop_sector = track.start_sector - 1
					previous_track.stop = track.start - 1
				}
			}
			else {
				eprintln('Unsupported command : ${command[0]}')
				exit(1)
			}
		}
	}

	// Update last track infos
	mut last_track := tracks[current_track]
	last_track.stop = bin_file_size - 1
	last_track.stop_sector = last_track.stop / sector_length

	return bin_file, tracks
}

fn run(cmd cli.Command) ! {
	cue_file := cmd.flags.get_string('cue')!
	raw_mode := cmd.flags.get_bool('raw')!
	psx_mode := cmd.flags.get_bool('psx')!
	wav_mode := cmd.flags.get_bool('wav')!
	swap_mode := cmd.flags.get_bool('swap')!
	verbose := cmd.flags.get_bool('verbose')!
	basename := cmd.flags.get_string('basename')!

	bin_filename, tracks := parse_cue_file(cue_file, wav_mode, raw_mode, psx_mode, verbose)!

	println('')
	println('Writing tracks:')
	println('')
	mut input := os.open(bin_filename) or {
		eprintln('Failed to open input file: ${err}')
		exit(1)
	}
	defer {
		input.close()
	}

	mut output_filename := ''
	if !basename.is_blank() {
		output_filename = basename
	} else {
		output_filename = os.file_name(bin_filename).trim_string_right(os.file_ext(bin_filename))
	}
	for track in tracks {
		write_track(mut input, track, output_filename, wav_mode, swap_mode, verbose)
	}
}
